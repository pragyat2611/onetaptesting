/*
* Real time private chatting app using Angular 2, Nodejs, mongodb and Socket.io
* @author Shashank Tiwari
*/

'use strict';

const routeHandler = require('./../handlers/route-handler');
const Multer = require('multer');
const { createWriteStream }  = require('fs');
const multiparty = require('multiparty');

class Routes{

	constructor(app){
		this.app = app;
		
        this.multer = Multer({
            storage: Multer.memoryStorage(),
            limits: {
              fileSize: 5 * 1024 * 1024 // no larger than 5mb, you can change as needed.
            }
          });
	}

	/* creating app Routes starts */
	appRoutes(){
		this.app.post('/usernameAvailable', routeHandler.userNameCheckHandler);

		this.app.post('/register', routeHandler.registerRouteHandler);

		this.app.post('/login', routeHandler.loginRouteHandler);

		this.app.post('/userSessionCheck', routeHandler.userSessionCheckRouteHandler);

		this.app.post('/getMessages', routeHandler.getMessagesRouteHandler);

		this.app.get('*', routeHandler.routeNotFoundHandler);	
		
		// this.app.post('/upload', this.multer.single('file'), routeHandler.uploadFileHandler);
		this.app.post('/upload', (req,res)=>{
			console.log("req.file", req.file)
			console.log("req.body", req.body)
			console.log("req.param", req.params)
			let form = new multiparty.Form()
			form.parse(req);
			// console.log("req.param", form)

			// form.on('part', (part)=>{
			// 	console.log("chunk part", part);  
			// 	// console.log("chunk buffer", part.buffer); 
			// 	part.on('data', (chunk)=>{
			// 	// console.log("read chunk", chunk.length);
			// 	}) 
			// 	part.pipe(createWriteStream(`./${part.filename}`))
			// 	part.pipe(createWriteStream(`./${part.filename}`))
			// 	.on('close', ()=>{
			// 	res.writeHead(200, {'Content-Type':'text/html'});
			// 	res.end(`<h1>File Uploaded ${part.filename}</h1>`)
			// 	})
			// })
			let container = []
			form.on('part', (part)=>{
				console.log("part", part)
				let uploadObj = {
					buffer : container,
					mimetype : part.headers['content-type'],
					size : part.byteCount - part.byteOffset,
					filename : part.filename
					
					}
				part.on('data', (chunk)=>{
					// console.log('chunk', chunk);
					container.push(JSON.stringify(chunk));
					// console.log("container buffer", container);

				})

				part.on('end', ()=>{
					console.log("part end", uploadObj)
					routeHandler.uploadFileHandler(uploadObj);
				})

			})
			form.on('close', ()=>{
				// console.log("container buffer", container);

					res.writeHead(200, {'Content-Type':'text/html'});
					res.end(`<h1>File Uploaded </h1>`)
			})
		});
		  
	}

	routesConfig(){
		this.appRoutes();
	}
}
module.exports = Routes;