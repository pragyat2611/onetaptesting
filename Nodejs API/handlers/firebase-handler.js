class FirebaseHandler {


    constructor() {
        
        this.googleStorage = require('@google-cloud/storage');
        this.firebaseSDK = require('firebase');
        this.queryBase = require('querybase');
        this.config = {
            apiKey: "AIzaSyAXAjbR9jtmb_RGoe0BYT2qe7WQ0MHNmZg",
            authDomain: "onetap-29476.firebaseapp.com",
            databaseURL: "https://onetap-29476.firebaseio.com",
            projectId: "onetap-29476",
            storageBucket: "onetap-29476.appspot.com",
            messagingSenderId: "579704768357"
        };
        this.firebaseSDK.initializeApp(this.config);
        this.rootRef = this.firebaseSDK.database().ref('/chat');
        this.usersRef = this.firebaseSDK.database().ref('/users');
        this.storage = new this.googleStorage.Storage({
            projectId: "onetap-29476",
            keyFilename: "config/onetap-29476-firebase-adminsdk-hjk9l-04a7f07b12.json"
          });

        this.bucket = this.storage.bucket("onetap-29476.appspot.com");


    }

    registerUser(data) {


        this.usersRef.update({
            [data.username]: {
                username: data.username,
                password: data.password,
                online: data.online,
                socketId: data.socketId
            }
        });
        return new Promise((resolve, reject) => {
            if (data) {
                resolve(data);
            }
        })
    }

    loginUser(username, password) {

        let querybaseRef = this.queryBase.ref(this.usersRef, ['username', 'password']);

        let queriedDbRef = querybaseRef
            .where({
                username: username,
                password: password
            });
        // Listen for realtime updates

        return new Promise((resolve, reject) => {
            queriedDbRef.on('value', snap => {
                resolve(username);
            }, err => {
                reject(err);
            });
        })

    }

    userSessionCheck(data) {
        return new Promise(async (resolve, reject) => {
            this.usersRef.orderByChild("username")
                .equalTo(data.username)
                .on("child_added", function (snapshot) {
                    resolve({ username: data.username });
                }, function (error) {
                    reject(err);
                });
        });
    }

    userNameCheck(data) {
        return new Promise(async (resolve, reject) => {
            try {

                this.usersRef.orderByChild("username")
                    .equalTo(data.username)
                    .on("child_added", function (snapshot) {
                        console.log(snapshot.numChildren());
                        resolve(snapshot.numChildren());

                    }, function (error) {
                        reject(error);
                    });


            } catch (error) {
                reject(error)
            }
        });
    }

    makeUserOnline(userId) {
        return new Promise(async (resolve, reject) => {

            try {
                this.usersRef.child(userId).update({ username: userId, online: 'Y' });
                resolve({ 'username': userId });
            } catch (error) {
                reject(error)
            }
        });
    }

    addSocketId({ userId, socketId }) {
        return new Promise(async (resolve, reject) => {

            try {
                this.usersRef.child(userId).update({ username: userId, socketId: socketId, online: 'Y' });
                resolve(userId);
            } catch (error) {
                reject(error)
            }
        });

    }

    logout(username, isSocketId) {

        return new Promise(async (resolve, reject) => {
            try {
                let condition = {};
                if (isSocketId) {
                    condition.socketId = username;
                } else {
                    condition.username = username;
                }

                this.usersRef.child(username).update({ username: username, socketId: condition.username, online: 'N' });
                resolve(username);

            } catch (error) {
                reject(error)
            }
        });
    }

    getChatList(socketId) {

        let totalUsers = [];
                    return new Promise((resolve, reject) => {   
                        this.usersRef.orderByKey().once('value')
                        .then(function (snapshot) {
                            snapshot.forEach(function (childSnapshot) {
                                totalUsers.push({'username' : childSnapshot.val().username,
                                                'id' : childSnapshot.val().socketId,
                                                'online': childSnapshot.val().online});
                            
                            });
                            resolve(totalUsers);
                        }
                        ).catch(err => reject(err))
                })
            }


    getUserInfo({ userId, socketId = false }) {


        return new Promise((resolve,reject) => {
            this.usersRef.orderByKey().equalTo(userId).once('value').then(snap => {
                resolve(snap.child(userId).val());
            }).catch(err => reject(err))
        })  
    }

    insertMessages(messagePacket){
        let primaryKey = messagePacket.fromUserId + '-' + messagePacket.toUserId;
        this.rootRef.update({
            [primaryKey]: {
                fromUserId: messagePacket.fromUserId,
                toUserId : messagePacket.toUserId,
                message : messagePacket.message
            }
        });
        return new Promise((resolve, reject) => {
            if (data) {
                resolve(data);
            }
        })
		
    }
    
    getMessages({userId, toUserId}){
        let querybaseRef = this.queryBase.ref(this.rootRef, ['toUserId', 'fromUserId']);

        let queriedDbRef = querybaseRef
            .where({
                toUserId: toUserId,
                fromUserId: userId
            });
        // Listen for realtime updates
            let totalMessages = [];
        return new Promise((resolve, reject) => {
            queriedDbRef.on('value', snap => {
                snap.forEach(ch => {
                    totalMessages.push(ch.val());
                })

                resolve(
                    totalMessages
                );
            }, err => {
                reject(err);
            });
        })
	}

    // uploadFileToStorage(file)  {
    //     let prom = new Promise((resolve, reject) => {
    //       if (!file) {
    //         reject('No image file');
    //       }
    //       let newFileName = `${file.originalname}_${Date.now()}`;
      
    //       let fileUpload = this.bucket.file(newFileName);
      
    //       const blobStream = fileUpload.createWriteStream({
    //         metadata: {
    //           contentType: file.mimetype
    //         }
    //       });
      
    //       blobStream.on('error', (error) => {
    //         reject('Something is wrong! Unable to upload at the moment.');
    //       });
      
    //       blobStream.on('finish', () => {
    //         // The public URL can be used to directly access the file via HTTP.
    //         const url = `https://storage.googleapis.com/${this.bucket.name}/${fileUpload.name}`;
    //         resolve(url);
    //       });
      
    //       blobStream.end(file.buffer);
    //     });
    //     return prom;
    //   }
    uploadFileToStorage(file)  {
        // console.log("file", file);
        let prom = new Promise((resolve, reject) => {
          if (!file) {
            reject('No image file');
          }
          let newFileName = `${file.filename}_${Date.now()}`;
      
          let fileUpload = this.bucket.file(newFileName);
        //   console.log("mimeType", file.mimetype)
          const blobStream = fileUpload.createWriteStream({
            metadata: {
              contentType: file.mimetype
            }
          });
         
          blobStream.on('error', (error) => {
              console.log('error', error)
            reject('Something is wrong! Unable to upload at the moment.');
          });
          
          
          blobStream.on('finish', () => {
            //   console.log('finish', fileUpload)
            // The public URL can be used to directly access the file via HTTP.
            const url = `https://storage.googleapis.com/${this.bucket.name}/${fileUpload.name}`;
            console.log("url", url)
            resolve(url);
          });
         
          blobStream.end(file.buffer.toString());

        });
        return prom;
      }

   

}

module.exports = new FirebaseHandler();


